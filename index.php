<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});

use clases\Persona;
use clases\Estudiante;
use clases\Materias;
use clases\Profesor;
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
            /* al crear el objeto le paso un array asociativo */
          $objeto=new Persona([
              "Nombre"=>"Mi primer alumno",
              "Edad"=>32,
            ]);
          
          
          var_dump($objeto);
          //echo $objeto;
          
          $oe=New Estudiante([]);
          var_dump($oe);
          
          $objetoEstudiante=new Estudiante([
            "Nombre"=>"alumno2", 
            "Edad"=>50,
            "Notas"=>[8,7,6],
            "Carrera"=>"Carrera"
            ]);
          var_dump($objetoEstudiante);
          
          $materia1=new Materias([
              "Nombre"=>"Mates"
          ]);
          
          $objetoProfesor=new Profesor([
              "Nombre"=>"Pepe",
              "Edad"=>56,
              "Materias"=>[new Materias([
                  "Nombre"=>"Fisica",
                  "Horas"=>10
                  ]),$materia1
                ],
              "Mes"=>10,
              "Sueldo"=>1200
          ]);
                    
          $materia1->setTitular($objetoProfesor);
          
          var_dump($objetoProfesor->getMaterias()[0]);
          var_dump($objetoProfesor->getMaterias()[0]->getTitular());
          //$objetoProfesor1=new Profesor()
          
          
                   
          
          
        ?>
    </body>
</html>
