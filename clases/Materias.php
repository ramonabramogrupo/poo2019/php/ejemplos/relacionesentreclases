<?php
namespace clases;

/**
 * Description of Materias
 *
 * @author ramon
 */
class Materias {
    public $nombre;
    private $horas;
    private $titular;
        
    function getNombre() {
        return $this->nombre;
    }

    function getHoras() {
        return $this->horas;
    }

    function setNombre(string $nombre) {
        $this->nombre = $nombre;
    }

    function setHoras(int $horas) {
        $this->horas = $horas;
    }
    
    public function getTitular() {
        return $this->titular;
    }

    public function setTitular(Profesor $titular=null) {
        $this->titular = $titular;
    }

        public function __construct($argumentos=[]) {
         $opcionales=[
             "Nombre"=>"",
             "Horas"=>0,
             /*"Titular"=>new Profesor([
                 "Nombre"=>"Profesor Titular por defecto",
                 "Edad"=>30,
                 "Materias"=>[$this],
             ]),*/
             "Titular"=>NULL,
         ];
        $argumentos= array_merge($opcionales,$argumentos);
        $this->setHoras($argumentos["Horas"]);
        $this->setNombre($argumentos["Nombre"]);
        $this->setTitular($argumentos["Titular"]);
    }


}
