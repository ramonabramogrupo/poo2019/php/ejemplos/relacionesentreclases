<?php

namespace clases;

/**
 * Description of Estudiante
 *
 * @author ramon
 */
class Estudiante extends Persona{
    private $carrera;
    public $notas=[];
    
    function getCarrera() {
        return $this->carrera;
    }

    /**
     * 
     * @return string,  devuelve las notas en string separadas por ,
     */
    function getNotas() {
        return join(",",$this->notas);
    }
    
    public function __toString(){
        return "Soy un estudiante";
    }

    
    function setCarrera(string $carrera) {
        $this->carrera = $carrera;
    }

    function setNotas(array $notas) {
        foreach($notas as $v){
            $this->notas[] = $v;
        }
        
        //array_push($this->notas,[$nota1,$nota2]); // cuidado con pasar varias notas como array
    }
      
    public function __construct($argumentos=[]) {
        $opcionales=[
            "Nombre"=>"",
            "Edad"=>0,
            "Notas"=>[],
            "Carrera"=>""
        ];
       
       /** logica de negocio **/
       $argumentos=array_merge($opcionales,$argumentos);
       $this->setCarrera($argumentos["Carrera"]);
       $this->setNotas($argumentos["Notas"]);
        parent::__construct([
            "Nombre"=>$argumentos["Nombre"],
            "Edad"=>$argumentos["Edad"]
        ]);
    }


}
